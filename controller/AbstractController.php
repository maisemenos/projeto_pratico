<?php

include_once 'ControllerSession.php';

abstract class AbstractController{
    
    private $session;
    
    
    public function __construct(){
        $this->session = new Session();
        self::startSession();
    }
    
    public function startSession(){
        self::getSession()->start();
    }
    
    public function getSession(){
      return $this->session;   
    }
    
    // Todo as classes filhas controller terão acesso a $session, ou seja, mesmo mudando de página a session estará ativa.
    
}


?>